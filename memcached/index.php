<?php
require './vendor/autoload.php';

$client = new \Clickalicious\Memcached\Client('127.0.0.1');
?>
<html>
<head>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
<div class="container">
    <h1 class="mt-5">
        Memcached
    </h1>
    <p>
    <?php
        $client->set('10324567', 'Luis Antonio Perea');
        echo 'result: '.$client->get('10324567');
    ?>
    </p>
    <p>
    <?php
        $client->set('objeto', '{"cedula": "10324567", "nombre": "Luis Antonio Perea"}');
        echo 'result: '. $client->get('objeto');
    ?>
    </p>
</div>
</body>
</html>
