
VERSION 4  MAJOR VERSION 3 SKIPPED
==================================

   Version 4.5 - MongoDB support
      23/10/2018 16:21  4.5.0  initial release
         e8c1589 Add ODM Entity
         407fbce Add suport to mongodb in trait

   Version 4.4 - Add Datetime Range
      19/10/2018 15:38  4.4.1  Add TimeShift
         fb98725 Add TimeShift
      10/10/2018 16:53  4.4.0  initial release
         3fba4c0 Add datetimeRange back()
         ba18984 Apply CS fix
         4a93d59 Add DatetimeRange
         b360334 Add export to Reflected
         a905724 Add phan tool
         8eb34d2 Add Makefile
         55e0526 Improve editor config

   Version 4.3 - Removed getDoctrine
      25/09/2018 10:22  4.3.1  Fix doctrine close
         b8a4fe6 Add new cs config
         4e9fd52 Rename doctrine getter
      24/09/2018 15:53  4.3.0  initial release

   Version 4.2 - Add Doctrine Manager Aware
      24/08/2018 15:20  4.2.3  Remove dev-master
         cef7abe Update dependency version
         3058694 find concrete getters and setters
         30acd7f snack case call fix
      01/08/2018 16:13  4.2.2  Improve messages
         35962bd Improve exception message
         3ed5e6b Improve accessor
      13/07/2018 09:58  4.2.1  Refactory
         27e0f33 Update alias
         2e3bf07 refactory
      05/07/2018 16:29  4.2.0  initial release
         8175dbe Fix conflicts
         dc05148 Apply cs rules
         1a919d0 add tests
         26819df add tests
         5bb11bb Add factoryEmptyAbsorbed
         c88c1c6 fix name
         e23dc16 add doctrine aware

   Version 4.1 - Add DateTime normalizer
      27/06/2018 08:28  4.1.1  add factoryElementAndAdd
         4212079 add abstract function to trait getter
         e618940 update console table style
         b53c10c Simplify CI config
         36732d7 simplify CI
      26/06/2018 15:27  4.1.0  initial release
         208ba15 Refactory color
         f7bda0d add factoryDateTimeByString
         834ab26 Add DateTime normalizer
         b636733 Add normalizer test
         89e2c31 add print color
         f4bacb1 refactory git-flow-absorb
         d6ed2fe add string methods
         e4bee4f fix alias
         6073dc0 add aliases
         fd60475 add git branch name function
         20c3449 Add git-flow funcions

   Version 4.0 - Major version 3 skipped
      14/06/2018 11:44  4.0.0  initial release

VERSION 3  MAJOR VERSION 4
==========================

   Version 3.0 - Major version 4
      14/06/2018 11:44  3.0.0  initial release

VERSION 2  LOGGER CHANNEL SWITCH
================================

   Version 2.7 - Add SimpleCache Aware Trait
      18/05/2018 17:15  2.7.1  Add id generator
      18/05/2018 13:47  2.7.0  initial release

   Version 2.6 - Inspect Tool
      17/05/2018 14:34  2.6.0  initial release
         aba95fc Add Inspect Tool

   Version 2.5 - Add suport to use displayTableResults() with generic collections
      10/05/2018 18:15  2.5.0  initial release
         6bc0a66 Add suport to use displayTableResults() with generic collections

   Version 2.4 - Add Reflected object
      03/05/2018 18:26  2.4.2  "Rename  getAbsorbed() to accessAbsorbed()"
         4c9af7c Rename getAbsorbed() to accessAbsorbed()
         8444ffb Add hasAbsorbed()
      26/04/2018 17:34  2.4.1  Add Decorated Tool
      20/04/2018 17:58  2.4.0  initial release
         d7b0965 Add Reflected object

   Version 2.3 - Absorbed Tool
      20/04/2018 13:03  2.3.2  Fix Absorbed Namespace
         98119b2 Fix Absorbed Namespace
      19/04/2018 19:14  2.3.1  AbsorbedAwareInterface
         da64144 Add AbsorbedAwareInterface
         8bc3fae Migrate to new scrutinizer-ci PHP Analysis #a
      18/04/2018 13:59  2.3.0  initial release
         24bae66 Add Absorbed Tool
         07f8d02 Update README

   Version 2.2 - Logger channel switch
      18/04/2018 11:35  2.2.0  initial release
         2f61bec Logger channel switch
         c3ed643 Better Exception message on acessor
         1e40fed Add RMT config