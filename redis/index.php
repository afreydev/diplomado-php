<?php
require './vendor/autoload.php';

use RedisClient\RedisClient;
use RedisClient\Client\Version\RedisClient2x6;
use RedisClient\ClientFactory;

// Example 1. Create new Instance for Redis version 2.8.x with config via factory
$Redis = ClientFactory::create([
    'server' => '127.0.0.1:6379', // or 'unix:///tmp/redis.sock'
    'timeout' => 2,
    'version' => '4.0.9'
]);

?>
<html>
<head>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
<div class="container">
    <h1 class="mt-5">
    <?php 
        echo 'RedisClient: '. $Redis->getSupportedVersion() . PHP_EOL; 
    ?>
    </h1>
    <p>
    <?php
        $Redis->executeRaw(['SET', '10324567', 'Luis Antonio Perea']);
        echo 'result: '. $Redis->executeRaw(['GET', '10324567']) .PHP_EOL; // bar
    ?>
    </p>
    <p>
    <?php
        $Redis->executeRaw(['SET', 'objeto', '{"cedula": "10324567", "nombre": "Luis Antonio Perea"}']);
        echo 'result: '. $Redis->executeRaw(['GET', 'objeto']) .PHP_EOL; // bar
    ?>
    </p>
</div>
</body>
</html>
